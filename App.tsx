import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Root } from './src/routers/Root';

// export default function App() {
//   return (
//     <View style={styles.container}>
//       {/* <Root></Root> */}
//       <Text style={{}}>Darova</Text>
//     </View>
//   );
// }

export default function App() {
  return (
    <Root/>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
