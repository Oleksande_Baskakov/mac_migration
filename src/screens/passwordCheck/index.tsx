import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { FC, useEffect, useState } from 'react';
import { FlatList, Linking, SafeAreaView, Text, TextInput, TouchableOpacity,Button, View, StyleSheet } from 'react-native';
import styles from './styles';

const Home: FC = (): JSX.Element => {
  const { navigate } = useNavigation();
  const onChangeText = (e) => {
    console.log(e);
  }
  return (
    <SafeAreaView style={styles.safeArea}>
       <View style={styles.container}>
            <Text>Darova. Please write parol</Text>
            <TextInput
              style={styles2.input}
              onChangeText={onChangeText}
      />
      </View>
      <Button title='daButton' onPress={() => navigate('List')}>Da</Button>
    </SafeAreaView>
  );
}
const styles2 = StyleSheet.create({
  input: {
    minWidth: 100,
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});

export default Home;
