import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { FC, useEffect, useState } from 'react';
import { SafeAreaView, Text, TextInput, View, StyleSheet, Button } from 'react-native';
import styles from './styles';

const Home: FC = (): JSX.Element => {
  const onChangeText = (e) => {
    console.log(e);
  }
  const { navigate } = useNavigation();
  return (
    <SafeAreaView style={styles.safeArea}>
       <View style={styles.container}>
            <Text>Darova. Please make login</Text>
            <TextInput
              style={styles2.input}
              onChangeText={onChangeText}        
      />
      <Button title='daButton' onPress={() => navigate('Pass')}>Da</Button>
      </View>
    </SafeAreaView>
  );
}
const styles2 = StyleSheet.create({
  input: {
    minWidth: 100,
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});

export default Home;
