import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LogIn from '../../screens/LogIn';
import Password from '../../screens/passwordCheck';
import List from '../../screens/List';

const AuthStack = createStackNavigator();

export const AuthRouter: React.FC = (): JSX.Element => {
  return (
    <AuthStack.Navigator>
        <AuthStack.Screen name='Log' component={LogIn}/>
        <AuthStack.Screen name='Pass' component={Password}/>
        <AuthStack.Screen name='List' component={List}/>
    </AuthStack.Navigator>
  );
}
